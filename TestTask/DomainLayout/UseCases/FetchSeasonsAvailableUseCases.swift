//
//  FetchSeasonsAvailableUseCases.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class FetchSeasonsAvailableUseCases: FetchDataUseCaseDelegate {
    
    typealias DataModel = SeasonsAvailableModel
    
    private var repository: SeasonsAvailableRepository
    
    init(repository: SeasonsAvailableRepository) {
        self.repository = repository
    }
    
    func fetchDataModel(_ completionHandler: @escaping FetchDataModelUseCaseCompletionHandler) {
        repository.fire { result in
            switch result {
            case .success(let dataModel):
                DispatchQueue.main.async {
                    completionHandler(Result.success(self.mapSeasonsAvailableModel(dataModel)))
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(Result.failure(error))
                }
                break
            }
        }
    }
    
    private func mapSeasonsAvailableModel(_ model: RemoteSeasonsAvailableModel) -> SeasonsAvailableModel {
        return SeasonsAvailableModel(
            status: model.status,
            data: mapDataClass(model.data)
        )
    }
    
    private func mapDataClass(_ model: RemoteSeasonsAvailableDataClass?) -> SeasonsAvailableDataClass {
        return SeasonsAvailableDataClass(
            desc: model?.desc,
            seasons: mapSeason(model?.seasons)
        )
    }
    
    private func mapSeason(_ models: [RemoteSeason]?) -> [Season]? {
        return models?.map { model in
            return Season(
                year: model.year,
                startDate: model.startDate,
                endDate: model.endDate,
                displayName: model.displayName,
                types: mapTypeElement(model.types)
            )
        }
    }
    
    private func mapTypeElement(_ model: [RemoteTypeElement]?) -> [TypeElement]? {
        return model?.map { model in
            return TypeElement(
                id: model.id,
                name: model.name,
                abbreviation: model.abbreviation,
                startDate: model.startDate,
                endDate: model.endDate,
                hasStandings: model.hasStandings)
        }
    }
}
