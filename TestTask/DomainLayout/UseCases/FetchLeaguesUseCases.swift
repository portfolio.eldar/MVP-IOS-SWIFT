//
//  FetchLeaguesUseCases.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct FetchLeaguesUseCases: FetchDataUseCaseDelegate {
    
    typealias DataModel = LeaguesModel
    
    private var repository: LeaguesRepository
    
    init(repository: LeaguesRepository) {
        self.repository = repository
    }
        
    func fetchDataModel(_ completionHandler: @escaping FetchDataModelUseCaseCompletionHandler) {
        repository.fire { result in
            switch result {
            case .success(let dataModel):
                DispatchQueue.main.async {
                    completionHandler(Result.success(mapLeagues(dataModel)))
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(Result.failure(error))
                }
                break
            }
        }
    }
    
    private func mapLeagues(_ model: RemoteLeaguesModel?) -> LeaguesModel {
        return LeaguesModel(
            status: model?.status,
            data: mapDatum(model?.data)
        )
    }
    
    private func mapDatum(_ models: [RemoteDatum]?) -> [Datum]? {
        return models?.map({ model in
            return Datum(
                id: model.id,
                name: model.name,
                slug: model.slug,
                abbr: model.abbr,
                logos: mapLogos(model.logos)
            )
        })
    }
    
    private func mapLogos(_ model: RemoteLogos?) -> Logos? {
        return Logos(
            light: model?.light,
            dark: model?.dark
        )
    }
}
