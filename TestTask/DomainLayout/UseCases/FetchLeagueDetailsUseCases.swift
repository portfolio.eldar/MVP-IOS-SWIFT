//
//  FetchLeagueDetailsUseCases.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class FetchLeagueDetailsUseCases: FetchDataUseCaseDelegate {
    
    typealias DataModel = LeagueDetailModel
    
    private var repository: LeagueDetailRepository
    
    init(repository: LeagueDetailRepository) {
        self.repository = repository
    }
    
    func fetchDataModel(_ completionHandler: @escaping FetchDataModelUseCaseCompletionHandler) {
        repository.fire { result in
            switch result {
            case .success(let dataModel):
                DispatchQueue.main.async {
                    completionHandler(Result.success(self.mapLeaguesDetails(dataModel)))
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(Result.failure(error))
                }
                break
            }
        }
    }
    
    private func mapLeaguesDetails(_ model: RemoteLeagueDetailModel) -> LeagueDetailModel {
        return LeagueDetailModel(
            status: model.status,
            data: mapDataClass(model.data)
        )
    }
    
    private func mapDataClass(_ model: RemoteDataClass?) -> DataClass {
        return DataClass(
            id: model?.id,
            name: model?.name,
            slug: model?.slug,
            abbr: model?.abbr,
            logos: mapLogos(model?.logos)
        )
    }
    
    private func mapLogos(_ model: RemoteLogos?) -> Logos? {
        return Logos(
            light: model?.light,
            dark: model?.dark
        )
    }
}
