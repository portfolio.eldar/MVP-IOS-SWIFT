//
//  FetchStandingsRepository.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class FetchStandingsUseCases: FetchDataUseCaseDelegate {
    
    typealias DataModel = StandingsModel
    
    private var repository: StandingsRepository
    
    init(repository: StandingsRepository) {
        self.repository = repository
    }
    
    func fetchDataModel(_ completionHandler: @escaping FetchDataModelUseCaseCompletionHandler) {
        repository.fire { result in
            switch result {
            case .success(let dataModel):
                DispatchQueue.main.async {
                    completionHandler(Result.success(self.mapStandingsModel(dataModel)))
                }
                break
            case .failure(let error):
                DispatchQueue.main.async {
                    completionHandler(Result.failure(error))
                }
                break
            }
        }
    }
    
    private func mapStandingsModel(_ model: RemoteStandingsModel) -> StandingsModel {
        return StandingsModel(
            status: model.status,
            data: mapStandingsDataClass(model.data)
        )
    }
    
    private func mapStandingsDataClass(_ model: RemoteStandingsDataClass?) -> StandingsDataClass {
        return StandingsDataClass(
            name: model?.name,
            abbreviation: model?.abbreviation,
            seasonDisplay: model?.seasonDisplay,
            season: model?.season,
            standings: mapStandings(model?.standings)
        )
    }
    
    private func mapStandings(_ models: [RemoteStanding]?) -> [Standing]? {
        return models?.map { model in
            return Standing(
                team: mapTeam(model.team),
                note: mapNote(model.note),
                stats: mapStats(model.stats)
            )
        }
    }
    
    private func mapStats(_ models: [RemoteStat]?) -> [Stat]? {
        return models?.map { model in
            return Stat(
                name: model.name,
                displayName: model.displayName,
                shortDisplayName: model.shortDisplayName,
                statDescription: model.statDescription,
                abbreviation: model.abbreviation,
                type: model.type,
                value: model.value,
                displayValue: model.displayValue,
                id: model.id,
                summary: model.summary
            )
        }
    }
    
    private func mapNote(_ model: RemoteNote?) -> Note? {
        return model.map { model in
            return Note(
                color: model.color,
                noteDescription: model.noteDescription,
                rank: model.rank
            )
        }
    }
    
    private func mapTeam(_ models: RemoteTeam?) -> Team? {
        return models.map { model in
            return Team(
                id: model.id,
                uid: model.uid,
                location: model.location,
                name: model.name,
                abbreviation: model.abbreviation,
                displayName: model.displayName,
                shortDisplayName: model.shortDisplayName,
                isActive: model.isActive,
                logos: mapLogo(model.logos)
            )
        }
    }
    
    private func mapLogo(_ models: [RemoteLogo]?) -> [Logo]? {
        return models?.map { model in
            return Logo(
                href: model.href,
                width: model.width,
                height: model.height,
                alt: model.alt,
                rel: model.rel,
                lastUpdated: model.lastUpdated
            )
        }
    }
}
