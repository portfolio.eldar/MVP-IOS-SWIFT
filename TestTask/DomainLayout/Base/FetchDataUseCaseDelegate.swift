//
//  FetchDataUseCaseDelegate.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

protocol FetchDataUseCaseDelegate {
    
    associatedtype DataModel
    
    typealias FetchDataModelUseCaseCompletionHandler = (_ books: Result<DataModel, Error>) -> ()
    func fetchDataModel(_ completionHandler: @escaping FetchDataModelUseCaseCompletionHandler)
}
