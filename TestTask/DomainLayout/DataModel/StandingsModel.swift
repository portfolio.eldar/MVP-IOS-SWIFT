//
//  StandingsModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct StandingsModel {
    let status: Bool?
    let data: StandingsDataClass?
}

struct StandingsDataClass {
    let name, abbreviation, seasonDisplay: String?
    let season: Int?
    let standings: [Standing]?
}

struct Standing {
    let team: Team?
    let note: Note?
    let stats: [Stat]?
}

struct Note {
    let color, noteDescription: String?
    let rank: Int?
}

struct Stat {
    let name: String?
    let displayName: String?
    let shortDisplayName: String?
    let statDescription: String?
    let abbreviation: String?
    let type: String?
    let value: Int?
    let displayValue, id, summary: String?
}

struct Team {
    let id, uid, location, name: String?
    let abbreviation, displayName, shortDisplayName: String?
    let isActive: Bool?
    let logos: [Logo]?
}

struct Logo {
    let href: String?
    let width, height: Int?
    let alt: String?
    let rel: [String]?
    let lastUpdated: String?
}
