//
//  LeaguesModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct LeaguesModel {
    let status: Bool?
    let data: [Datum]?
}

struct Datum {
    let id, name, slug, abbr: String?
    let logos: Logos?
}
