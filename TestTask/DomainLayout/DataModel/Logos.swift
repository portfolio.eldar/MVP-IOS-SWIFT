//
//  Logos.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct Logos {
    let light, dark: String?
}
