//
//  SeasonsAvailableModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct SeasonsAvailableModel {
    let status: Bool?
    let data: SeasonsAvailableDataClass?
}

struct SeasonsAvailableDataClass {
    let desc: String?
    let seasons: [Season]?
}

struct Season {
    let year: Int?
    let startDate, endDate, displayName: String?
    let types: [TypeElement]?
}

struct TypeElement {
    let id, name, abbreviation, startDate: String?
    let endDate: String?
    let hasStandings: Bool?
}
