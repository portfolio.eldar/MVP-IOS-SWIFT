//
//  LeagueDetails.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct LeagueDetailModel {
    let status: Bool?
    let data: DataClass?
}

struct DataClass {
    let id, name, slug, abbr: String?
    let logos: Logos?
}

