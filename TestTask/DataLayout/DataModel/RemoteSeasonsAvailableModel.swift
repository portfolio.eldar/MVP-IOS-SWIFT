//
//  RemoteSeasonsAvailableModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct RemoteSeasonsAvailableModel: Codable {
    let status: Bool?
    let data: RemoteSeasonsAvailableDataClass?
}

struct RemoteSeasonsAvailableDataClass: Codable {
    let desc: String?
    let seasons: [RemoteSeason]?
}

struct RemoteSeason: Codable {
    let year: Int?
    let startDate, endDate, displayName: String?
    let types: [RemoteTypeElement]?
}

struct RemoteTypeElement: Codable {
    let id, name, abbreviation, startDate: String?
    let endDate: String?
    let hasStandings: Bool?
}
