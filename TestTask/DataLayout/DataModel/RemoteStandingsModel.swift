//
//  RemoteStandingsModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct RemoteStandingsModel: Codable {
    let status: Bool?
    let data: RemoteStandingsDataClass?
}

struct RemoteStandingsDataClass: Codable {
    let name, abbreviation, seasonDisplay: String?
    let season: Int?
    let standings: [RemoteStanding]?
}

struct RemoteStanding: Codable {
    let team: RemoteTeam?
    let note: RemoteNote?
    let stats: [RemoteStat]?
}

struct RemoteNote: Codable {
    let color, noteDescription: String?
    let rank: Int?

    enum CodingKeys: String, CodingKey {
        case color
        case noteDescription = "description"
        case rank
    }
}

struct RemoteStat: Codable {
    let name: String?
    let displayName: String?
    let shortDisplayName: String?
    let statDescription: String?
    let abbreviation: String?
    let type: String?
    let value: Int?
    let displayValue, id, summary: String?

    enum CodingKeys: String, CodingKey {
        case name, displayName, shortDisplayName
        case statDescription = "description"
        case abbreviation, type, value, displayValue, id, summary
    }
}

struct RemoteTeam: Codable {
    let id, uid, location, name: String?
    let abbreviation, displayName, shortDisplayName: String?
    let isActive: Bool?
    let logos: [RemoteLogo]?
}

struct RemoteLogo: Codable {
    let href: String?
    let width, height: Int?
    let alt: String?
    let rel: [String]?
    let lastUpdated: String?
}
