//
//  LeaguesModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct RemoteLeaguesModel: Codable {
    let status: Bool?
    let data: [RemoteDatum]?
}

struct RemoteDatum: Codable {
    let id, name, slug, abbr: String?
    let logos: RemoteLogos?
}
