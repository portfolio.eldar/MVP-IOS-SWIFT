//
//  RemoteLogos.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct RemoteLogos: Codable {
    let light, dark: String?
}
