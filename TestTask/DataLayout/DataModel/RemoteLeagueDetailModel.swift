//
//  RemoteLeagueDetailModel.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

struct RemoteLeagueDetailModel: Codable {
    let status: Bool?
    let data: RemoteDataClass?
}

struct RemoteDataClass: Codable {
    let id, name, slug, abbr: String?
    let logos: RemoteLogos?
}
