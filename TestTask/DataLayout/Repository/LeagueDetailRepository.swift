//
//  LeagueDetailRepository.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class LeagueDetailRepository: NetworkFetchable {
    
    typealias DataModel = RemoteLeagueDetailModel
    
    private var id: String
    
    private lazy var urlRequest = URLRequest(url: URL(string: "https://api-football-standings.azharimm.site/leagues/\(id)")!)
    
    init(_ id: String) {
        self.id = id
    }
    
    func fire(_ completionHandler: @escaping (Result<RemoteLeagueDetailModel, NetworkError>) -> ()) {
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            do {
                if let data = data {
                    let model = try JSONDecoder().decode(DataModel.self, from: data)
                    
                    completionHandler(Result.success(model))
                } else {
                    completionHandler(Result.failure(.emptyContent))
                }
            } catch {
                completionHandler(Result.failure(.serviceError))
            }
        }.resume()
    }

}
