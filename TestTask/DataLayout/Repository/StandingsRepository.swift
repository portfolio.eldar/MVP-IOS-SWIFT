//
//  StandingsRepository.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class StandingsRepository: NetworkFetchable {
    
    typealias DataModel = RemoteStandingsModel
    
    private var age: Int
    private var id: String
    
    private lazy var urlRequest = URLRequest(url: URL(string: "https://api-football-standings.azharimm.site/leagues/\(id)/standings?season=\(age)&sort=asc")!)
    
    init(_ id: String, _ age: Int) {
        self.age = age
        self.id = id
    }
    
    func fire(_ completionHandler: @escaping (Result<RemoteStandingsModel, NetworkError>) -> ()) {
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            do {
                if let data = data {
                    let model = try JSONDecoder().decode(DataModel.self, from: data)
                    
                    completionHandler(Result.success(model))
                } else {
                    completionHandler(Result.failure(.emptyContent))
                }
            } catch {
                completionHandler(Result.failure(.serviceError))
            }
        }.resume()
    }
}
