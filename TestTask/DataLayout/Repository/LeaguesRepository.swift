//
//  LeaguesRepository.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class LeaguesRepository: NetworkFetchable {
    
    typealias DataModel = RemoteLeaguesModel
    
    private let urlRequest = URLRequest(url: URL(string: "https://api-football-standings.azharimm.site/leagues")!)
    
    func fire(_ completionHandler: @escaping (Result<RemoteLeaguesModel, NetworkError>) -> ()) {
        URLSession.shared.dataTask(with: urlRequest) { data, response, error in
            do {
                if let data = data {
                    let model = try JSONDecoder().decode(DataModel.self, from: data)
                    
                    completionHandler(Result.success(model))
                } else {
                    completionHandler(Result.failure(.emptyContent))
                }
            } catch {
                completionHandler(Result.failure(.serviceError))
            }
        }.resume()
    }
}
