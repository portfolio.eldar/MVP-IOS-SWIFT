//
//  NetworkFetchable.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

enum NetworkError: Error {
    case url
    case encode
    case connection
    case decode
    case emptyContent
    case serviceError
}

protocol NetworkFetchable {
    
    associatedtype DataModel: Codable
    
    func fire(_ completionHandler: @escaping (Result<DataModel, NetworkError>) -> ())
}
