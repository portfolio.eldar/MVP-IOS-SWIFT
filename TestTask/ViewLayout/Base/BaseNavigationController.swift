//
//  BaseNavigationController.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpNavBar()
        setUpUI()
    }
    
    func setUpUI() {
        view.backgroundColor = .white
    }
    
    func setUpNavBar() {
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.white
        navigationBar.backgroundColor = UIColor.white
        navigationBar.isTranslucent = false
        
//        isNavigationBarHidden = true
    }
}
