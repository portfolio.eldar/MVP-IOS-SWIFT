//
//  BasePresenter.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation

protocol BasePresenter: AnyObject {
    var router: RouterProtocol? { get set }
    
    init(router: RouterProtocol)
}
