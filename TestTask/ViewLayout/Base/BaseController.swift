//
//  BaseController.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation
import UIKit

class BaseController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        view.backgroundColor = .white
    }
}
