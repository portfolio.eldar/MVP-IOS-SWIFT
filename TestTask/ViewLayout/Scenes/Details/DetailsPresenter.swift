//
//  DetailsPresenter.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

protocol DetailsPresenterDelegate: BasePresenter {
    var view: DetailsViewDelegate? { get set }
    
    func getLeagueDetails()
    func getSeasonsAvailable()
    
    func showStandings(age: Int?)
    
    init(leaguesUseCases: FetchLeagueDetailsUseCases, seasonUseCases: FetchSeasonsAvailableUseCases, router: RouterProtocol, id: String)
}

class DetailsPresenter: DetailsPresenterDelegate {
    
    var router: RouterProtocol?
    var view: DetailsViewDelegate?
    
    private var leaguesUseCases: FetchLeagueDetailsUseCases? = nil
    private var seasonUseCases: FetchSeasonsAvailableUseCases? = nil
    
    private var id: String? = nil
    
    required init(
        leaguesUseCases: FetchLeagueDetailsUseCases,
        seasonUseCases: FetchSeasonsAvailableUseCases,
        router: RouterProtocol,
        id: String
    ) {
            
        self.leaguesUseCases = leaguesUseCases
        self.seasonUseCases = seasonUseCases
            
        self.id = id
        
        self.router = router
    }
    
    required init(router: RouterProtocol) {
        self.router = router
    }
    
    func getLeagueDetails() {
        leaguesUseCases?.fetchDataModel { books in
            switch books {
            case .success(let dataModel):
                self.view?.loadDetails(model: dataModel)
                break
            case .failure(let error):
                
                print(error)
                
                break
            }
        }
    }
    
    func getSeasonsAvailable() {
        seasonUseCases?.fetchDataModel { books in
            switch books {
            case .success(let dataModel):
                self.view?.loadSeasonsAvailable(model: dataModel)
                break
            case .failure(let error):
                
                print(error)
                
                break
            }
        }
    }
    
    func showStandings(age: Int?) {
        if let age = age {
            if let id = id {
                router?.showStandings(id: id, age: age)
            }
        }
    }
}
