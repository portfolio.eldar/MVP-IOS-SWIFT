//
//  DetailsController.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation
import UIKit

protocol DetailsViewDelegate: AnyObject {
    func loadDetails(model: LeagueDetailModel)
    func loadSeasonsAvailable(model: SeasonsAvailableModel)
}

class DetailsController: BaseController {
    
    var presenter: DetailsPresenterDelegate? = nil
    
    private lazy var imageLeague: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private lazy var titleView: UILabel = {
        let view = UILabel()
        view.textAlignment = .center
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        return view
    }()
    
    private lazy var seasonsTableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    private var model: SeasonsAvailableModel? = nil
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        view.addSubview(imageLeague)
        imageLeague.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(4)
            make.left.right.equalToSuperview()
            make.height.equalTo(100)
        }
        
        view.addSubview(titleView)
        titleView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(imageLeague.snp.bottom).offset(4)
        }
        
        view.addSubview(seasonsTableView)
        seasonsTableView.snp.makeConstraints { make in
            make.top.equalTo(titleView.snp.bottom).offset(8)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        presenter?.getLeagueDetails()
        presenter?.getSeasonsAvailable()
    }
}

extension DetailsController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.data?.seasons?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = model?.data?.seasons?[indexPath.row]

        presenter?.showStandings(age: model?.year)
        
        seasonsTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let model = model?.data?.seasons?[indexPath.row]
        
        cell.textLabel?.text = model?.displayName
        cell.textLabel?.numberOfLines = 0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension DetailsController: DetailsViewDelegate {
    func loadSeasonsAvailable(model: SeasonsAvailableModel) {
        self.model = model
        
        seasonsTableView.reloadData()
    }
    
    func loadDetails(model: LeagueDetailModel) {
        if let url = URL(string: model.data?.logos?.light ?? String()) {
            imageLeague.kf.setImage(with: url)
        }
        
        titleView.text = model.data?.name
    }
}
