//
//  DetailsBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class DetailsBuilder: ViewBuilder {
    
    typealias ViewType = DetailsController
    
    static func build(router: RouterProtocol, id: String) -> DetailsController {
        let repository = LeagueDetailRepository(id)
        let useCases = FetchLeagueDetailsUseCases(repository: repository)
        
        let seasonsAvailableRepository = SeasonsAvailableRepository(id)
        let seasonsAvailableUseCases = FetchSeasonsAvailableUseCases(repository: seasonsAvailableRepository)
        
        let controller = DetailsController()
        let presenter = DetailsPresenter(leaguesUseCases: useCases, seasonUseCases: seasonsAvailableUseCases, router: router, id: id)
        
        controller.presenter = presenter
        controller.presenter?.view = controller
        
        return controller
    }
}
