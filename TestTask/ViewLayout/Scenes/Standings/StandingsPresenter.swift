//
//  StandingsPresenter.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

protocol StandingsPresenterDelegate: BasePresenter {
    var view: StandingsViewDelegate? { get set }

    init(useCases: FetchStandingsUseCases, router: RouterProtocol)
    
    func getStandings()
}

class StandingsPresenter: StandingsPresenterDelegate {

    var view: StandingsViewDelegate?
    
    var router: RouterProtocol?
    
    private var useCases: FetchStandingsUseCases? = nil
    
    required init(useCases: FetchStandingsUseCases, router: RouterProtocol) {
        self.router = router
        self.useCases = useCases
    }
    
    required init(router: RouterProtocol) {
        self.router = router
    }
    
    func getStandings() {
        useCases?.fetchDataModel({ books in
            switch books {
            case .success(let dataModel):
                self.view?.loadStandings(model: dataModel)
                
                break
            case .failure(let error):
                
                print(error)
                
                break
            }
        })
    }
}
