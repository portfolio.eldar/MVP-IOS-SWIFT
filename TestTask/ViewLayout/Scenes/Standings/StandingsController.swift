//
//  StandingsController.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation
import UIKit

protocol StandingsViewDelegate: AnyObject {
    func loadStandings(model: StandingsModel)
}

class StandingsController: BaseController {
    
    var presenter: StandingsPresenterDelegate? = nil
    
    private lazy var standingsTableView: UITableView = {
        let view = UITableView()
        view.dataSource = self
        view.delegate = self
        return view
    }()
    
    private var model: StandingsModel? = nil
    
    override func viewDidLoad() {
        view.backgroundColor = .white
        
        view.addSubview(standingsTableView)
        standingsTableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        title = "Standings"
        
        presenter?.getStandings()
    }
}

extension StandingsController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.data?.standings?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = LeagueCell()
        let model = model?.data?.standings?[indexPath.row]
        
        cell.fill(model)
        
        return cell
    }
}

extension StandingsController: StandingsViewDelegate {
    func loadStandings(model: StandingsModel) {
        self.model = model
        
        standingsTableView.reloadData()
    }
}
