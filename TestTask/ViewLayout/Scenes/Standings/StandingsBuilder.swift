//
//  StandingsBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation

class StandingsBuilder: ViewBuilder {
    typealias ViewType = StandingsController
    
    static func build(router: RouterProtocol, age: Int, id: String) -> StandingsController {
        let repository = StandingsRepository(id, age)
        let useCases = FetchStandingsUseCases(repository: repository)
        
        let controller = StandingsController()
        let presenter = StandingsPresenter(useCases: useCases, router: router)
        
        controller.presenter = presenter
        controller.presenter?.view = controller
        
        return controller
    }
}
