//
//  MainController.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

protocol MainViewDelegate: AnyObject {
    func showLeaguesList(models: LeaguesModel)
}

class MainController: BaseController {
    
    var presenter: MainPresenterDelegate? = nil
    
    private var models: LeaguesModel? = nil
    
    private lazy var leaguesTableView: UITableView = {
        let view = UITableView()
        
        view.delegate = self
        view.dataSource = self
        view.separatorStyle = .none
        
        view.register(LeagueCell.self, forCellReuseIdentifier: "LeagueCell")
        
        return view
    }()
    
    override func viewDidLoad() {
        view.addSubview(leaguesTableView)
        leaguesTableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        navigationItem.hidesBackButton = true
        title = "Leagues"
        
        presenter?.getLeaguesList()
    }
}

extension MainController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = models?.data?[indexPath.row]

        presenter?.showDetails(id: model?.id)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueCell", for: indexPath) as! LeagueCell
        let model = models?.data?[indexPath.row]
        
        cell.fill(model)
        
        return cell
    }
}

extension MainController: MainViewDelegate {
    func showLeaguesList(models: LeaguesModel) {
        self.models = models
        
        leaguesTableView.reloadData()
    }
}
