//
//  LeagueCell.swift
//  TestTask
//
//  Created by Eldar Akkozov on 20.05.2022.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

class LeagueCell: UITableViewCell {
    
    private var imageLeague: UIImageView = {
        let view = UIImageView()
        
        return view
    }()
    
    private var title: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    private var subTitle: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14)
        view.numberOfLines = 0
        return view
    }()
    
    
    override func layoutSubviews() {
        addSubview(imageLeague)
        imageLeague.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
            make.height.width.equalTo(100)
        }
        
        addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.left.equalTo(imageLeague.snp.right).offset(10)
        }
        
        addSubview(subTitle)
        subTitle.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(4)
            make.right.equalToSuperview().offset(-10)
            make.left.equalTo(imageLeague.snp.right).offset(10)
        }
    }
    
    func fill(_ model: Datum?) {
        if let url = URL(string: model?.logos?.light ?? String()) {
            imageLeague.kf.setImage(with: url)
            
            layoutIfNeeded()
        }
        
        title.text = model?.abbr
        subTitle.text = model?.name
    }
    
    func fill(_ model: Standing?) {
        if (model?.team?.logos?.count ?? 0) > 0 {
            if let url = URL(string: model?.team?.logos?[0].href ?? String()) {
                imageLeague.kf.setImage(with: url)
                
                layoutIfNeeded()
            }
        }
        
        title.text = model?.team?.displayName
    }
}
