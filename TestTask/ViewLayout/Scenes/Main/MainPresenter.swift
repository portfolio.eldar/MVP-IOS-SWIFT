//
//  MainPresenter.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation

protocol MainPresenterDelegate: BasePresenter {
    var view: MainViewDelegate? { get set }

    init(leaguesUseCases: FetchLeaguesUseCases, router: RouterProtocol)

    func showDetails(id: String?)
    func getLeaguesList()
}

class MainPresenter: MainPresenterDelegate {
    
    var router: RouterProtocol?
    var view: MainViewDelegate?
    
    var leaguesUseCases: FetchLeaguesUseCases?
    
    required init(leaguesUseCases: FetchLeaguesUseCases, router: RouterProtocol) {
        self.router = router
        self.leaguesUseCases = leaguesUseCases
    }
    
    required init(router: RouterProtocol) {
        self.router = router
    }
    
    func showDetails(id: String?) {
        if let id = id {
            router?.showDetails(id: id)
        }
    }
    
    func getLeaguesList() {
        leaguesUseCases?.fetchDataModel { [weak self] books in
            switch books {
            case .success(let models):
                self?.view?.showLeaguesList(models: models)
                break
            case .failure(_):
                
                break
            }
        }
    }
}
