//
//  MainBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation

class MainBuilder: ViewBuilder {
    
    typealias ViewType = MainController

    static func build(router: RouterProtocol) -> MainController {
        let repository = LeaguesRepository()
        let useCases = FetchLeaguesUseCases(repository: repository)
        
        let presenter = MainPresenter(leaguesUseCases: useCases, router: router)
        let controller = MainController()
        
        controller.presenter = presenter
        controller.presenter?.view = controller
        
        return controller
    }
}
