//
//  Splash.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

protocol SplashViewDelegate: AnyObject {  }

class SplashController: BaseController {
    
    var presenter: SplashPresenterDelegate? = nil
    
    override func viewDidLoad() {        
        presenter?.checkNavigations()
    }
}

extension SplashController: SplashViewDelegate { }
