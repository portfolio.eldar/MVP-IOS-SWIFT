//
//  SplashPresenter.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation

protocol SplashPresenterDelegate: BasePresenter {
    var view: SplashViewDelegate? { get set }
    
    func checkNavigations()
}

class SplashPresenter: SplashPresenterDelegate {
    
    var router: RouterProtocol?
    var view: SplashViewDelegate? = nil

    required init(router: RouterProtocol) {
        self.router = router
    }
    
    func checkNavigations() {
        router?.showMain()
    }
}
