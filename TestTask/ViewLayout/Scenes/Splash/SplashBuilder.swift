//
//  SplashBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation

class SplashBuilder: ViewBuilder {
    
    typealias ViewType = SplashController
    
    static func build(router: RouterProtocol) -> SplashController {
        let presenter = SplashPresenter(router: router)
        let controller = SplashController()
        
        controller.presenter = presenter
        controller.presenter?.view = controller
        
        return controller
    }
}
