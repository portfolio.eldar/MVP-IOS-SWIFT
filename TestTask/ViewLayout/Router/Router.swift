//
//  Router.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

protocol RouterMain {
    var navigationController: UINavigationController? { get set }
    var builder: BuilderProtocol? { get set }
    
    init(navigationController: UINavigationController, builder: BuilderProtocol)
}

protocol RouterProtocol: RouterMain  {
    func initialSplashViewContrller()
    func showMain()
    func showDetails(id: String)
    func showStandings(id: String, age: Int)
    func popToRoot()
}

class Router: RouterProtocol {
    
    var navigationController: UINavigationController?
    var builder: BuilderProtocol?
    
    required init(navigationController: UINavigationController, builder: BuilderProtocol) {
        self.navigationController = navigationController
        self.builder = builder
    }
    
    func initialSplashViewContrller() {
        guard let navigationController = navigationController else { return }
        guard let controller = builder?.createSplashModule(router: self) else { return }
        
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showMain() {
        guard let navigationController = navigationController else { return }
        guard let controller = builder?.createMainModule(router: self) else { return }

        navigationController.pushViewController(controller, animated: true)
    }
    
    func popToRoot() {
        guard let navigationController = navigationController else { return }
        navigationController.popToRootViewController(animated: true)
    }
    
    func showDetails(id: String) {
        guard let navigationController = navigationController else { return }
        guard let controller = builder?.createDetailsModule(router: self, id: id) else { return }

        navigationController.pushViewController(controller, animated: true)
    }
    
    func showStandings(id: String, age: Int) {
        guard let navigationController = navigationController else { return }
        guard let controller = builder?.createStandingsModule(router: self, id: id, age: age) else { return }

        navigationController.pushViewController(controller, animated: true)
    }
}
