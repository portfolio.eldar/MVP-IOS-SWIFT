//
//  ViewBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

protocol ViewBuilder {
    associatedtype ViewType: UIViewController
}
