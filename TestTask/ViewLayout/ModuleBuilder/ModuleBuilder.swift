//
//  ModuleBuilder.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import Foundation
import UIKit

protocol BuilderProtocol {
    func createSplashModule(router: RouterProtocol) -> UIViewController
    func createMainModule(router: RouterProtocol) -> UIViewController
    func createDetailsModule(router: RouterProtocol, id: String) -> UIViewController
    func createStandingsModule(router: RouterProtocol, id: String, age: Int) -> UIViewController
}

class ModuleBuilder: BuilderProtocol {
    func createStandingsModule(router: RouterProtocol, id: String, age: Int) -> UIViewController {
        return StandingsBuilder.build(router: router, age: age, id: id)
    }
    
    func createDetailsModule(router: RouterProtocol, id: String) -> UIViewController {
        return DetailsBuilder.build(router: router, id: id)
    }
    
    func createSplashModule(router: RouterProtocol) -> UIViewController {
        return SplashBuilder.build(router: router)
    }
    
    func createMainModule(router: RouterProtocol) -> UIViewController {
        return MainBuilder.build(router: router)
    }
}
