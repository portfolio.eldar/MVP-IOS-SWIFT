//
//  AppDelegate.swift
//  TestTask
//
//  Created by Eldar Akkozov on 19.05.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        regularLaunching()

        return true
    }

    private func regularLaunching() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.overrideUserInterfaceStyle = .light
        
        let navigationController = BaseNavigationController()
        let builder = ModuleBuilder()
        let router = Router(navigationController: navigationController, builder: builder)
        
        router.initialSplashViewContrller()
        
        self.window!.rootViewController = navigationController
        self.window!.makeKeyAndVisible()
    }
}

