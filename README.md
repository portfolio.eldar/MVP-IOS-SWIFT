# MVP

This is a project to showcase `Clean Architecture` at `MVP` (Model-View-Presenter

# Architecture

![](https://gitlab.com/portfolio.eldar/TestTask/-/raw/main/TestTask/FlowChart/Screen%20Shot%202022-05-20%20at%2021.12.22.png)

## View Layer (Model-View-Presenter)
- `View` - Displays information from the `Presenter` and sends user interactions back to the `Presenter`.
- `Presenter` - Contains the presentation logic and tells the `View` what to present
- `ViewBuilder` - The `Builder’s` responsibility is to instantiate a specific `View` and injects the dependency for all components.
- `Router` - Handles navigation logic for which screen should appear and when.

## Domain Layer
- `UseCase` - Contains the business logic for a specific use case in the project. They are view agnostic and can be consumed by one or many `Presenters`.
- `Model` - Simple data model objects.

## Data Layer
- `Repository` - Query objects from different data sources (Core Data, Realm, web server, etc.) with a only single-entry point.
